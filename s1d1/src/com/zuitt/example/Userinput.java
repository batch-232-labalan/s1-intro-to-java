package com.zuitt.example;

import java.util.Scanner;

public class Userinput {
    public static void main(String[] args){
        //scanner class
        //is like a prompt() in js

        //System.in - take input from console
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username");

        String username = myObj.nextLine();
        System.out.println("Username is: " + username);

    }
}
