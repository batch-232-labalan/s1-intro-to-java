package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // variable
        int age;
        char middle_name;

        //variable init
        int x = 0;
        age = 18;

        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        //float
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        //char - can only hold one character
        //char - uses single quotes
        char letter = 'a';
        System.out.println(letter);

        //boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        //constants in Java
        //final variable datatype VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //string - non primitive data type. string ar actually objects in Java that can use methods
        String username = "theTinker23";
        System.out.println(username);

        //isEmpty - checks the length of the string, returns true if the is 0, returns false otherwise
        System.out.println(username.isEmpty());
    }
}
